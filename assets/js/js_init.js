(function($) {
	$(document).ready(function() {
		// Document Ready

		$.each($('.ms_feed'), function() {
			var feed = $(this);
			var url = feed.attr('data-url');
			var max = feed.attr('data-max');
			var offset = feed.attr('data-offset');

			var wrapperClass = feed.attr('data-wrapper-class');
			var ulClass = feed.attr('data-ul-class');
			var liClass = feed.attr('data-li-class');
			var aClass = feed.attr('data-a-class');
			var namespace = feed.attr('data-namespace');
			var specificTag = feed.attr('data-specifig-tag');

			var data = {
				"action" : "readFeed",
				url : url,
				max : max,
				offset : offset,
				wrapper : wrapperClass,
				ulClass : ulClass,
				liClass : liClass,
				aClass : aClass,
				namespace : namespace,
				specificTag : specificTag
			};

			jQuery.ajax({
				type : "post",
				dataType : "json",
				url : feedAjax.handler,
				data : data,
				success : function(response) {
					// console.log(response);

					if (response.type == "success") {
						feed.html(response.output)
					} else {
						feed.html('Error...')
					}
				}
			});
			// Document Ready ends
		});

	});
}(jQuery));