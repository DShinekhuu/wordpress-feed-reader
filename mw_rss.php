<?php

defined('ABSPATH') or die("No script kiddies please!");
/**
 * Plugin Name: Metawise RSS Reader
 * Plugin URI: http://website.com
 * Description: Custom RSS Reader.
 * Version: 1.0
 * Author: Shinekhuu
 * Author URI: http://orondoo.com
 * License: GPL2
 */

require 'init.php';