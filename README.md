# README #

### Shortcode attributes ###


```
#!php

'url' => '',  // Example: http://www.federalreserve.gov/feeds/Data/H15_H15_RIFLGFCY02_N.B.XML
'max' => '5', // Max items
'offset' => '0', // Offset 
'preloader' => 'circle', // Options: circle|square|(custom word, for example: Loading, please wait...)
'wrapper_class' => '', // Wrapper div class
'ul_class' => '', // ul list class
'li_class' => '', // li class
'a_class' => '', // link class
'namespace' => '', // XML tag namespace. Must have two elements. 1 namespace tag, 2 namespace URL. Example : cb|http://www.cbwiki.net/wiki/index.php/Specification_1.1  ( See above URL example XML file )
'specific_tag' => '' // Multi level - statistics|otherStatistic|value  , single level value
```


### Example shortcode usage ###

* Multi level tag
[mw_rss wrapper-class="myWrapper" url="http://www.federalreserve.gov/feeds/Data/H15_H15_RIFLGFCY02_N.B.XML" ul_class="myUl"  li_class="myli" a_class="mya" preloader="circle" wrapper_class="asdasd" namespace="cb|http://www.cbwiki.net/wiki/index.php/Specification_1.1" specific_tag="statistics|value" ]

* Different namespace with single level tag

[mw_rss wrapper-class="myWrapper" url="http://www.federalreserve.gov/feeds/Data/H15_H15_RIFLGFCY02_N.B.XML" ul_class="myUl"  li_class="myli" a_class="mya" preloader="circle" wrapper_class="asdasd" namespace="dc|http://purl.org/dc/elements/1.1/" specific_tag="date" ]

### Contributors ###

d.shinekhuu@gmail.com