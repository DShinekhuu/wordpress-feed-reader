<?php
// mimic the actuall admin-ajax
define('DOING_AJAX', true);

if (! isset($_POST['action']))
    die('-1');
    
    // define('SHORTINIT', true);
require_once ('../../../../wp-load.php');

// Typical headers
header('Content-Type: application/json');
// send_nosniff_header();

// Disable caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');

// A bit of security
$allowed_actions = array(
    'readFeed'
);

// For logged in users
add_action('ss_readFeed', 'readFeed');

// For guests
add_action('ss_nopriv_readFeed', 'readFeed');

$action = $_POST['action'];

if (in_array($action, $allowed_actions)) {
    if (is_user_logged_in())
        do_action('ss_' . $action);
    else
        do_action('ss_nopriv_' . $action);
} else {
    die('-1');
}

// Actions
function readFeed()
{
    $data = $_POST;
    // $action = $data['action'];
    
    include_once (ABSPATH . WPINC . '/feed.php');
    
    $url = $data['url'];
    $max = $data['max'];
    $offset = $data['offset'];
    
    $wrapperClass = $data['wrapperClass'];
    $ulClass = $data['ulClass'];
    $liClass = $data['liClass'];
    $aClass = $data['aClass'];
    $specificTag = $data['specificTag'];
    $namespace = $data['namespace'];
    
    $rss = fetch_feed($url);
    
    if (! is_wp_error($rss)) { // Checks that the object is created correctly
                               
        // Figure out how many total items there are, but limit it to 5.
        $maxitems = $rss->get_item_quantity($max);
        
        // Build an array of all the items, starting with element 0 (first element).
        $rss_items = $rss->get_items($offset, $maxitems);
    }
    
    $namespace = explode("|", trim($namespace));
    $specificTag = explode("|", trim($specificTag));
    
    ob_start();
    ?>

<div <?php ($wrapperClass!='')?'class="'.$wrapperClass.'"':'""';?>>
	<ul class="<?php echo $ulClass;?>">
        <?php if ( $maxitems == 0 ) : ?>
            <li class="<?php echo $liClass;?>"><?php _e( 'No items', 'my-text-domain' ); ?></li>
        <?php else : ?>
            <?php // Loop through each feed item and display each item as a hyperlink. ?>
            <?php
        foreach ($rss_items as $item) :
            
            ?>
                <li class="<?php echo $liClass;?>"><a
			class="<?php echo $aClass;?>"
			href="<?php echo esc_url( $item->get_permalink() ); ?>"
			title="<?php printf( __( 'Posted %s', 'my-text-domain' ), $item->get_date('j F Y | g:i a') ); ?>">
                        <?php
            
            // If namespace & specific tags declared
            if (count($specificTag) > 0 && $specificTag[0] != '' && count($namespace) > 1) {
                
                $cb = $namespace[1];
                $tags = $item->get_item_tags($cb, $specificTag[0]);
                
                // Has child ( multi level )
                if (count($tags[0]) > 5) {
                    findXyz($tags, $specificTag[count($specificTag) - 1]);
                } else { // Single level
                    echo $tags[0]['data'];
                }
            } else { // Show general title
                $tags = array();
                echo esc_html($item->get_title());
            }
            // XML Structure. Don't delete
            // echo 'Decimals: ' . $tags[0]['child'][$cb]['otherStatistic'][0]['child'][$cb]['value'][0]['attribs']['']['decimals'] . '<br>';
            // echo 'Value: ' . $tags[0]['child'][$cb]['otherStatistic'][0]['child'][$cb]['topic'][0]['data'] . '<br>';
            // echo 'Topic: ' . $tags[0]['child'][$cb]['otherStatistic'][0]['child'][$cb]['value'][0]['data'] . '<br>';
            
            ?>
                    </a> <br></li>
            <?php
        endforeach
        ;
        
        ?>
    
        <?php endif; ?>
    </ul>
</div>
<?php
    
    $output = ob_get_clean();
    
    $data['output'] = $output;
    $data['type'] = 'success';
    
    echo json_encode($data);
    die();
}

// Multidimensional array traverse - Lookup a key
function findXyz($array, $key)
{
    $output;
    foreach ($array as $foo => $bar) {
        if (is_array($bar)) {
            if ($bar[$key]) {
                echo $bar[$key][0]['data'];
                return;
            } else {
                findXyz($bar, $key);
            }
        }
    }
}
