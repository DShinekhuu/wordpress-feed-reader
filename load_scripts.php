<?php
add_action('wp_enqueue_scripts', 'feed_loadScripts');

function feed_loadScripts()
{
    wp_enqueue_style('mw_rss_style', plugins_url('/assets/css/style.css', __FILE__));
    
    wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js', array(), '1.11.1', true);
    wp_enqueue_script('jquery');
    
    wp_register_script('feed_init', plugins_url('/assets/js/js_init.js', __FILE__), array(
        'jquery'
    ), false, true);
    wp_enqueue_script('feed_init');
    
    // in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
    wp_localize_script('feed_init', 'feedAjax', array(
        'handler' => plugins_url('/ajax/ajax_handler.php', __FILE__)
    ));
}