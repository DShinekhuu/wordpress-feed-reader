<?php
defined('ABSPATH') or die("No script kiddies please!");

function sc_rss($atts)
{
    $a = shortcode_atts(array(
        'url' => '',
        'max' => '5',
        'offset' => '0',
        'preloader' => 'circle',
        'wrapper_class' => '',
        'ul_class' => '',
        'li_class' => '',
        'a_class' => '',
        // 'namespace' => 'cb|http://www.cbwiki.net/wiki/index.php/Specification_1.1', - DEPRESSED
        // 'specific_tag' => 'statistics|otherStatistic|value' - DEPRESSED
        // namespace="cb|http://www.cbwiki.net/wiki/index.php/Specification_1.1" specific_tag="value"
        // namespace="dc|http://purl.org/dc/elements/1.1/" specific_tag="date"
        'namespace' => '',
        'specific_tag' => ''
    ), $atts);
    
    if ($content)
        $a['url'] = $content;
    
    $output = '';
    
    $preloader = $a['preloader'];
    
    if ($preloader == 'circle') {
        $preloader = '<img class="feed-preloader" src="' . plugins_url('../assets/image/35.GIF', __FILE__) . '">';
    } elseif ($preloader == 'square') {
        $preloader = '<img class="feed-preloader" src="' . plugins_url('../assets/image/719.GIF', __FILE__) . '">';
    }
    
    $output .= '<div class="ms_feed" data-url="' . $a['url'] . '" data-max="' . $a['max'] . '" data-offset="' . $a['offset'] . '" 
        data-wrapper-class="' . $a['wrapper_class'] . '" data-ul-class="' . $a['ul_class'] . '" data-li-class="' . $a['li_class'] . '" 
            data-a-class="' . $a['a_class'] . '" data-title-tag="' . $a['title_tag'] . '" 
                data-specifig-tag="' . $a['specific_tag'] . '" data-namespace="' . $a['namespace'] . '">
        ' . $preloader . '</div>';
    
    return $output;
}

add_shortcode('mw_rss', 'sc_rss');
