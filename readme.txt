=== Wordpress Ajax RSS Feed reader ===
Contributors: jabga
Tags: ajax, rss, feed
Tested up to: 4.0
License: MIT
License URI: http://opensource.org/licenses/MIT

This plugin reads RSS Feed from given URL after page loaded, so it won\'t increase page loading time.
Can retrieve custom tags and XML namespaces.

== Description ==
This plugin reads RSS Feed from given URL after page loaded, so it won\'t increase page loading time.
Can retrieve custom tags and XML namespaces.

Shortcode guide can be seen here https://bitbucket.org/DShinekhuu/wordpress-feed-reader

Download: https://bitbucket.org/DShinekhuu/wordpress-feed-reader/downloads

Shortcode attributes: 
\'url\' => \'\',  // Example: http://www.federalreserve.gov/feeds/Data/H15_H15_RIFLGFCY02_N.B.XML
\'max\' => \'5\', // Max items
\'offset\' => \'0\', // Offset 
\'preloader\' => \'circle\', // Options: circle|square|(custom word, for example: Loading, please wait...)
\'wrapper_class\' => \'\', // Wrapper div class
\'ul_class\' => \'\', // ul list class
\'li_class\' => \'\', // li class
\'a_class\' => \'\', // link class
\'namespace\' => \'\', // XML tag namespace. Must have two elements. 1 namespace tag, 2 namespace URL. Example : cb|http://www.cbwiki.net/wiki/index.php/Specification_1.1  ( See above URL example XML file )
\'specific_tag\' => \'\' // Multi level - statistics|otherStatistic|value  , single level value

Example shortcode usage
Multi level tag [mw_rss wrapper-class=\"myWrapper\" url=\"http://www.federalreserve.gov/feeds/Data/H15_H15_RIFLGFCY02_N.B.XML\" ul_class=\"myUl\"  li_class=\"myli\" a_class=\"mya\" preloader=\"circle\" wrapper_class=\"asdasd\" namespace=\"cb|http://www.cbwiki.net/wiki/index.php/Specification_1.1\" specific_tag=\"statistics|value\" ]
Different namespace with single level tag
[mw_rss wrapper-class=\"myWrapper\" url=\"http://www.federalreserve.gov/feeds/Data/H15_H15_RIFLGFCY02_N.B.XML\" ul_class=\"myUl\"  li_class=\"myli\" a_class=\"mya\" preloader=\"circle\" wrapper_class=\"asdasd\" namespace=\"dc|http://purl.org/dc/elements/1.1/\" specific_tag=\"date\" ]

== Installation ==
Install plugin and add shortcode  [mw_rss url=\"XML_URL\"]

== Screenshots ==
1. http://i.imgur.com/F7XMCfV.png
2. http://i.imgur.com/wuPLfkD.png